/**
 * The following is the C function
 * @brief I forgot how much I hate C naming conventions but having unsigned data type are an A+
 * @brief Will clean up in future versions
 */

// Constants
#define FB_BLACK 	0
#define FB_GREEN 	2
#define FB_DARK_GREY 	8

// Framebuffer
char *fb = (char *) 0x000B8000;


/**
 * Writes a character with a given foreground and background to position i
 * in the framebuffer
 *
 * @param i  The location in the framebuffer
 * @param c  The char
 * @param fg The foreground color
 * @param bg The background color
 */
void fb_write_cell(unsigned int i, char c, unsigned char fg, unsigned char bg) {
 	fb[i] = c;
 	fb[i + 1] = ((fg & 0x0F) << 4) | (bg & 0x0F);
}

/**
 * Will call fb_write_cell and (clear) frame
 */
void fb_clean_cell() {
	unsigned int i;		// i can be used as a pointer
	unsigned int size = 1000;	// unsure the size
	
	for (i = 0; i < size; i++) {
		fb_write_cell(i*2, ' ', FB_BLACK, FB_BLACK);
	}
}

/**
 * Will call fb_write_cell and print char array
 */
void fb_print_cell() {
	char str[] = "Written in God's language, by Eddie Ray";
	unsigned int size = sizeof(str);
	unsigned int i;	// i can be used as a pointer
	
	for (i = 0; i < size; i++) {
		fb_write_cell(i*2, str[i], FB_BLACK, FB_DARK_GREY);
	} 
}


/**
 * Adds 3 values and returns the results
 *
 * @param arg1 1
 * @param arg2 2
 * @param arg3 3
 * @return the sum of (arg1 + arg2 + arg3)
 */
int sum_of_three(int arg1, int arg2, int arg3) {
	return (arg1 + arg2 + arg3);
}

int main() {
	//fb_write_cell(0, 'A', FB_GREEN, FB_DARK_GREY);
	//fb_clean_cell();
	//fb_print_cell();
	
}

int display(char *argv, int arg2) {
	unsigned int size = sizeof(argv);
	unsigned int i;
	//unsigned int total = 0;
	//unsigned int number = arg2;
	fb_clean_cell();
	for (i = 0; i < size; i++) {
		fb_write_cell(i*2, argv[i], FB_BLACK, FB_DARK_GREY);
	}
	return arg2;
}

	
